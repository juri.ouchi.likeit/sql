SELECT
	c.category_name,
	SUM(i.item_price) AS total_price
FROM
	item_category c
INNER JOIN
	item i
ON
	c.category_id = i.category_id
GROUP BY
	c.category_name
ORDER BY
	total_price DESC;
